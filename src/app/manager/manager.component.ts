import { Component, OnInit } from '@angular/core';
import { EmployeeComponent } from '../employee/employee.component';

export class Emp {
  name: string;
  mgrName: string;
}

export const Employees = [
  {name: 'Dr IQ',mgrName:'Sunil'},
  {name: 'Magneta',mgrName:'Ankita'},
  {name: 'Bombasto',mgrName:'Swati'}
];

@Component({
  selector: 'app-manager',
  template:  `
  <div><h4> No of employees {{employees.length}}</h4></div>
  <h4>Agree: {{agreed}}, Disagree: {{disagreed}}</h4>
  <app-employee *ngFor="let emp of employees"
    [emp1]="emp"> (voted)="onVoted($event)"
  </app-employee>
`,
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {
  employees = Employees;
  agreed = 0;
  disagreed = 0;
  

  onVoted(agreed: boolean) {
    console.log('onVoted');
    agreed ? this.agreed++ : this.disagreed++;
  }
  constructor() { }

  ngOnInit() {
  }

}
