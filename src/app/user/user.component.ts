import { Component, OnInit } from '@angular/core';
export class Todo{
  constructor(
    public id:number,
    public description:string,
    public status:boolean,
    public targetDate:Date
  ){}
}
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  todolist = [new Todo(1,'description1',false,new Date("2016-01-17T08:44:29+0100")),
  new Todo(1,'description2',true,new Date("2016-01-17T08:44:29+0100")),
  new Todo(1,'description3',false,new Date("2016-01-17T08:44:29+0100")),
  new Todo(1,'description4',true,new Date("2016-01-17T08:44:29+0100")),
  new Todo(1,'description5',false,new Date("2016-01-17T08:44:29+0100"))
  ];
  // todolist = {
  //   id:1,
  //   name:'qwe'
  // }
    constructor() { }
  
    ngOnInit() {
    }
}
