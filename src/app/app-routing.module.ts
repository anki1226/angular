import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LogInComponent} from './login/login.component';
import{HomeComponent} from './home/home.component';
import{AboutComponent} from './about/about.component';
import{UserComponent} from './user/user.component';
import{LogoutComponent} from'./logout/logout.component';
const routes: Routes = [
  {path: 'login', component: LogInComponent},
  {path: 'home', component: HomeComponent},
  {path: 'home/:name', component: HomeComponent},
  {path: 'about', component: AboutComponent},
  {path: 'user', component: UserComponent},
  {path: 'logout', component: LogoutComponent},
//  {path: '', redirectTo: '/login',pathMatch:'full'}
  ]



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
