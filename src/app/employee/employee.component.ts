import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ManagerComponent } from '../manager/manager.component';

export class Emp {
  name: string;
}

@Component({
  selector: 'app-employee',
  template: `<h4>{{emp1.name}} says:</h4>
    <p>I, {{emp1.name}}, am at your service, {{emp1.mgrName}}.</p>
    <button class="btn btn-primary" (click)="vote(true)" >Agree</button>
    <button class="btn btn-secondary" (click)="vote(false)" >Disagree</button>`,
  
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  @Input() emp1: Emp;
  // @Input('manager1') managerName: string;
  @Output() voted = new EventEmitter<boolean>();
  didVote = false;
  constructor() { }
  vote(agreed: boolean) {
    console.log("vote");
    this.voted.emit(agreed);
    //this.didVote = true;
  }
  ngOnInit() {
  }

}
