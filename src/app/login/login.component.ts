import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../service/authentication.service';
@Component({
  selector: 'app-log-in',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LogInComponent implements OnInit {
username = "xyz";
password = "abc";
errorMsg="Please provide username and password";
invalidLogin = false;
message = "Invalid credential";
  constructor(private router: Router,private authService:AuthenticationService) { }

  ngOnInit() {
  }

logIn(){
  if(this.authService.authenticate(this.username,this.password))
  {
    
    this.router.navigate(['home',this.username]);
    this.errorMsg="Login sucessfully";
  }
  else{
    //this.errorMsg="Please Provide correct password";
    this.invalidLogin = true;
  }
 // console.log("welcome "+this.username);
  //console.log("welcome pass "+this.password);
 // console.log("msg console");
}
// logOut(){
  
//    let user = sessionStorage.getItem('authenticatedUser');
//     return !(user===null);
// }
}
